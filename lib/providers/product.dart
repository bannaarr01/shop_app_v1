import 'package:flutter/material.dart';

///Product Model
class Product with ChangeNotifier {
  final String id;
  final String title;
  final String description;
  final double price;
  final String imageUrl;

  ///We don't store ds as asset baked into our application cos if we do, den we will need 2 ship a new app version wit every new products dt was added den dt's not feasible at all.
  bool isFavorite; //since ds will change
  //for Widgets dt depend on SINGLE product 2 rebuild, we use ds mixin

  ///ds not final cos it's changeable after d product has been created.

  Product({
    required this.id,
    required this.title,
    required this.description,
    required this.price,
    required this.imageUrl,
    this.isFavorite = false,
  });

  void toggleFavoriteStatus() {
    isFavorite = !isFavorite;
    notifyListeners();
    //🔊📢 Like set State in d provider package, let listening widget knows that something change n they should rebuild, equivalent to setState in stateFul Wgt
  }
}
