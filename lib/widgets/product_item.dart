import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/product.dart';
import '../screens/product_detail_screen.dart';
import '../providers/cart.dart';

class ProductItem extends StatelessWidget {
  // final String id;
  // final String title;
  // final String imageUrl;
  // ProductItem(
  //     {Key? key, required this.id, required this.title, required this.imageUrl})
  //    : super(key: key); all d above nt needed anymore since im usin provider hia
  ProductItem({Key? key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    //listen is default to True
    final product = Provider.of<Product>(context, listen: false);
    final cart = Provider.of<Cart>(context, listen: false);
    //debug
    // print('product rebuilding');
    //using Consumer insead of ds is same
    //return Container();/ card
    //Consumer has

    //To add rounded shapes ClipRoundedRectangle
    return ClipRRect(
      //it enforce ds on d child/children
      borderRadius: BorderRadius.circular(10),
      //Work particularly well inside of grid
      child: GridTile(
        //Create an image widget dt takes a ntwrk source
        // can put url Image.network('https://g.com/j.jpg)
        //GestureDetector allows us 2 add an onTap 2 Img
        child: GestureDetector(
            //So here when the user tap, we getting d new screen on d FLY🚀 dynamically.
            onTap: () {
              //MaterialPageRoute is a widget built into Flutter, which takes a builder method n giv a new context, an object which has 2 return a widget u want 2 go to. so hia we goin to
              // product detail
              // Navigator.of(context).push(
              //   MaterialPageRoute(
              //     builder: (ctx) => ProductDetailScreen(title: title),
              //   ),
              // );

              //🟢 NOW using route Instead of creating on the fly 🟢
              Navigator.of(context).pushNamed(
                ProductDetailScreen.routeName,
                arguments: product.id,
                //Forward d id to my named route, not all d data i need dea
              );
            },
            child: Image.network(
              product.imageUrl,
              fit: BoxFit.cover,
            )),
        //styling footer
        footer: GridTileBar(
          //black with opacity
          backgroundColor: Colors.black87,
          //Adding favorite icon button
          leading: Consumer<Product>(
            //Since ds is d only part dt listen 2 update
            builder: (ctx, product, _) => IconButton(
              // builder: (ctx, product, child) => IconButton(
              //what'sup with ,child👆🏻 If u have parts in ur widget, which den does update, which u don't actually want to rebuild wen it generally rebuild ds.👇(child in consumer )
              //mayb ref here as label: child, frm below. It will Not reBuild
              icon: Icon(
                  product.isFavorite ? Icons.favorite : Icons.favorite_border),
              onPressed: () {
                //Handling isFavorite / NOT
                product.toggleFavoriteStatus();
                //print('isFavorite rebuilding');
              },
              color: Theme.of(context).colorScheme.secondary,
            ),
            //4 testing, d child here can also b a complex widget tree
            //child: const Text('Never change'),
          ),
          //pass in d item title
          title: Text(
            product.title,
            textAlign: TextAlign.center,
          ),
          //Adding shopping cart icon button
          trailing: IconButton(
            icon: const Icon(Icons.shopping_cart),
            onPressed: () {
              cart.addItem(product.id, product.title, product.price);
              //confirm to user d item is added to the shopping cart
              //we establish a conenction 2 d nearest scaffold widget i.e the nearest widget that controls d page we're seeing(NEAREST ONE is in d product overview)
              // if the above return is scaffold den ds won't work (same widget tree).
              //can use ds 2 open drawer if d nearest has drawer
              // Scaffold.of(context).openDrawer();
              //ToaST Message
              //if there is new, d old one will b hidden n a new 1 shows
              ScaffoldMessenger.of(context).hideCurrentSnackBar();
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: const Text(
                    'Added item to cart!',
                    textAlign: TextAlign.center,
                  ),
                  duration: const Duration(seconds: 2),
                  action: SnackBarAction(
                    label: 'UNDO',
                    onPressed: () {
                      //Remove the just added item when Pressed Undo
                      cart.removeSingleItem(product.id);
                    },
                  ),
                ),
              );
            },
            color: Theme.of(context).colorScheme.secondary,
          ),
        ),
      ),
    );
  }
}

//DONT USE PROVIDER/CONSUMER IF U ONLY WANT TO CHANGE HOW SOMETHING LOOK OR IS DISPLAY IN A WIDGET 👉 App Wide State n Local State 👈 Master it well
