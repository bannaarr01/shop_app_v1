import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/products_provider.dart';

class ProductDetailScreen extends StatelessWidget {
  // final String title;
  // final double price; //doesnt need ds 2 props again since nw we av  Provider

  const ProductDetailScreen({Key? key}) : super(key: key);
  static const routeName = '/product-detail';
  @override
  Widget build(BuildContext context) {
    //Now Extract the ID pased as an arg down here
    final productId = ModalRoute.of(context)?.settings.arguments as String;
    //..Get all the product data for That ID here, which won't work if we always passing tru constructor
    //Now we need a Central State Management SOLUTION which is PROVIDER'
    //Now let Find the Product with d ID provided above
    // final loadedProduct = Provider.of<Products>(context).items.firstWhere(
    //       (prod) => prod.id == productId,
    //     ); Better done in the products class and Do always move as much logic away from Widgets.
    //Dont rebuild ds widget when d screen is build 4 d first time, instead Tap into d data one time since we only GET 👉 na NOT UPDATE
    final loadedProduct =
        Provider.of<Products>(context, listen: false).findById(productId);

    //using Scaffold, since ds will control full screen
    return Scaffold(
      appBar: AppBar(
        title: Text(loadedProduct.title),
      ),
      body: SingleChildScrollView(
        child: Column(
          children: <Widget>[
            Container(
              height: 300,
              width: double.infinity,
              child: Image.network(
                loadedProduct.imageUrl,
                fit: BoxFit.cover,
              ),
            ),
            const SizedBox(height: 10),
            Text(
              '\$${loadedProduct.price}',
              style: const TextStyle(
                color: Colors.grey,
                fontSize: 20,
              ),
            ),
            const SizedBox(height: 10),
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 10),
              width: double.infinity,
              child: Text(
                loadedProduct.description,
                textAlign: TextAlign.center,
                softWrap: true,
              ),
            )
          ],
        ),
      ),
    );
  }
}
