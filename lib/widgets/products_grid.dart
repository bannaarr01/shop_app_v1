import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/products_provider.dart';
import './product_item.dart';

class ProductsGrid extends StatelessWidget {
  final bool showFavs;
  ProductsGrid({Key? key, required this.showFavs}) : super(key: key);

  // final List<Product> loadedProducts;
  //Set Up a LISTENER INSTEAD

  @override
  //only from here will rebuild wen onbject it LISTEN to changes
  Widget build(BuildContext context) {
    //ds setup a direct communication channel bhin d scene, the parent of ds which is ProductOverview will not rebuild cos we not settin a listener there
    //the of is generic type <>
    final productsData = Provider.of<Products>(context);
    final products = showFavs ? productsData.favoriteItems : productsData.items;
    //GridBuilder is like ListViewBuilder but optimized longer grid views with multiple items or where we don't know how many items we have
    return GridView.builder(
      ///const 4 nt rebuild ds padding
      padding: const EdgeInsets.all(10.0),
      //Tells grid how many items to build
      itemCount: products.length,
      //function which rciv d context n index of d item it's currently
      /// building a cell for dt sud return d widget dt get built for every grid item we have
      itemBuilder: (ctx, index) => ChangeNotifierProvider.value(
        //this will return a single product item as its stored in d producs class n
        //it will do ds multiple times cos its inside of d item builder for all d product.
        // create: (ctx) => products[index],
        //but don't use .value 2 create object
        value: products[index],
        child: ProductItem(
            // products[index].id,
            // products[index].title,
            // products[index].imageUrl,
            ),
        //provider still ends up in memory unlike jes screen navitagtion dt flutter auto clean in memory. So displose d data u r storing here, HOWEVER ChangeNotifierProvider cleans it Up
        //ProductItem widget - Instead of putting everything here
      ),
      //grid delegate allow us 2 define how grid generally sud be structured, hw many columns it sud have
      gridDelegate: const SliverGridDelegateWithFixedCrossAxisCount(
        //2 columns
        crossAxisCount: 2,
        //be a bit higher dan dea wide, a bit taller dan dey r wide
        childAspectRatio: 3 / 2,
        //The spacing between the columns
        crossAxisSpacing: 10,
        //spacing btw d rows
        mainAxisSpacing: 10,
      ),
    );
  }
}
