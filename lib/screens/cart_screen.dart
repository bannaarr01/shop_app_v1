import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/cart.dart' show Cart;
import '../widgets/cart_item.dart';
import '../providers/orders.dart';

//for Name ambiguity import '../widgets/cart_item.dart' as ci; ci.then when wanted to use, can do ci.CartItem,
//or import '../providers/cart.dart' show Cart; this import Only Cart since there is 2 classes there and we are only interested in the Cart class, so there won't be name ambiguity for d widget own nd ds
class CartScreen extends StatelessWidget {
  static const routeName = '/cart';
  const CartScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final cart = Provider.of<Cart>(context);
    return Scaffold(
      appBar: AppBar(
        title: const Text('Your Cart'),
      ),
      body: Column(
        children: <Widget>[
          Card(
            margin: const EdgeInsets.all(15),
            child: Padding(
              padding: const EdgeInsets.all(8),
              child: Row(
                //spacing btw
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  const Text(
                    'Total',
                    style: TextStyle(fontSize: 20),
                  ),
                  //takes d available all d available spaces n reserve it 4itsef
                  Spacer(),
                  const SizedBox(width: 10),
                  //like our badge or label, element with rounded corner used 2 display information
                  //or Text(cart.totalAmount.toString()) d below 👇
                  //call .toString() automatically \$ dollar sign
                  Chip(
                      label: Text(
                        '\$${cart.totalAmount.toStringAsFixed(2)}',
                        style: TextStyle(color: Colors.white),
                      ),
                      backgroundColor: Theme.of(context).colorScheme.primary),
                  TextButton(
                    child: const Text('ORDER NOW'),
                    style: TextButton.styleFrom(primary: Colors.green),
                    onPressed: () {
                      Provider.of<Orders>(context, listen: false).addOrder(
                          cart.items.values.toList(), cart.totalAmount);
                      //after ordering, then clear cart
                      cart.clearCart();
                    },
                  ),
                  //Have Items below the total row
                  const SizedBox(height: 10),
                  //ListView won't work inside the column, so need 2 wrap it with expanded widget.
                ],
              ),
            ),
          ),
          Expanded(
            // Instead of doing all here, extract d widget n import
            child: ListView.builder(
              itemCount: cart.items.length,
              itemBuilder: (ctx, index) => CartItem(
                id: cart.items.values.toList()[index].id,
                productId: cart.items.keys.toList()[index],
                price: cart.items.values.toList()[index].price,
                quantity: cart.items.values.toList()[index].quantity,
                title: cart.items.values.toList()[index].title,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
