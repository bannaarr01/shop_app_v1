import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import '../providers/products_provider.dart';
import '../providers/product.dart';

//stateful cos need 2 manage user input n manage it here
class EditProductScreen extends StatefulWidget {
  static const routeName = '/edit-product';
  const EditProductScreen({Key? key}) : super(key: key);

  @override
  _EditProductScreenState createState() => _EditProductScreenState();
}

class _EditProductScreenState extends State<EditProductScreen> {
  final _priceFocusNode = FocusNode();
  final _descriptionFocusNode = FocusNode();
  final _imageController = TextEditingController();
  final _imageUrlFocusNode = FocusNode();
  //Interact n get all d form values
  final _form = GlobalKey<FormState>();

  var _editedProduct = Product(
    id: '',
    title: '',
    price: 0,
    description: '',
    imageUrl: '',
  );

  var _initValues = {
    'title': '',
    'description': '',
    'price': '',
    'imageUrl': '',
  };

//to run a logic when only needed cos didChangeDependencies Do RUN multiple times
  var _isInit = true;

  @override
  void initState() {
    // ModalRoute.of(context).settings ds doesnt work here 2 get argument
    // setup a listener
    _imageUrlFocusNode.addListener(_updateImageUrl);
    super.initState();
  }

  //ds also runs bf build is executed
  @override
  void didChangeDependencies() {
    if (_isInit) {
      //the ID passed from userproductItem
      // final productId = ModalRoute.of(context)?.settings.arguments as String; OR
      final productId = ModalRoute.of(context)!.settings.arguments == null
          ? "NULL"
          : ModalRoute.of(context)!.settings.arguments as String;

      //print('pid' + productId != 'NULL');
      if (productId != 'NULL') {
        //using d id to find the product n Assign it 2 editedProduct immediately
        final product =
            Provider.of<Products>(context, listen: false).findById(productId);
        _editedProduct = product;
        //Now need 2 initialize d form with default values
        //Using Strings cos d texfields only works with string
        //print('Edited Product ID NOT EMPTY:  ${_editedProduct.isFavorite}');
        // print('Edited Product ID NOT EMPTY:  ${_editedProduct.id}');
        _initValues = {
          'title': _editedProduct.title,
          'description': _editedProduct.description,
          'price': _editedProduct.price.toString(),
          // 'imageUrl': _editedProduct.imageUrl,
          'imageUrl': '',
        };
        //now set d image url HERE
        _imageController.text = _editedProduct.imageUrl;
      }
    }
    _isInit = false;
    super.didChangeDependencies();
  }

//rebuild d screen 2 reflect d updated Image Url
  void _updateImageUrl() {
    // if (!_imageUrlFocusNode.hasFocus) {
    //   setState(() {});
    // }
    if (!_imageUrlFocusNode.hasFocus) {
      if (_imageController.text.isEmpty ||
          (!_imageController.text.startsWith('http') &&
              !_imageController.text.startsWith('https')) ||
          (!_imageController.text.endsWith('.png') &&
              !_imageController.text.endsWith('.jpg') &&
              !_imageController.text.endsWith('.jpeg'))) {
        return; //stops
      }
      setState(() {});
    }
  }

  //they have 2 be cleared when leaving ds screen so dt dey don't cause mem leak
  @override
  void dispose() {
    _imageUrlFocusNode.removeListener(_updateImageUrl);
    _priceFocusNode.dispose();
    _descriptionFocusNode.dispose();
    _imageController.dispose();
    _imageUrlFocusNode.dispose();

    super.dispose();
  }

  //save form input fields
  void _saveForm() {
    final productId = ModalRoute.of(context)!.settings.arguments == null
        ? "NULL"
        : ModalRoute.of(context)!.settings.arguments as String;
    //trigger all d validation
    final isValid = _form.currentState?.validate();
    if (!isValid!) {
      return; //stops here and don't save if isValid is F
    }
    //save trigger a method on every form fields n allows u 2 take d value entered into d text field and do with it watever u want, i.e store it in Global map that collects all text inputs.

    _form.currentState?.save();

    if (_editedProduct.id.isNotEmpty) {
      //then ds product exist before, Update it
      // print('Edited Product ID NOT EMPTY:  ${_editedProduct.isFavorite}');
      // print('Edited Product ID NOT EMPTY:  ${_editedProduct.id}');
      // print("Edited Product ID: " + _editedProduct.id.toString());
      // print(
      //     '${_editedProduct.title}, ${_editedProduct.price}, ${_editedProduct.description}, ${_editedProduct.imageUrl}, ${_editedProduct.id}');
      Provider.of<Products>(context, listen: false)
          .updateProduct(_editedProduct.id, _editedProduct);
      Navigator.of(context).pop();
    } //otherwise we are adding product
    else {
      // print("Edited Product ID: " + _editedProduct.id.toString());
      //Add product 2 product List inside provider container
      Provider.of<Products>(context, listen: false).addProduct(_editedProduct);
      //Leave ds Page back to d prev Page
      Navigator.of(context).pop();
      // print(
      //     '${_editedProduct.title}, ${_editedProduct.price}, ${_editedProduct.description}, ${_editedProduct.imageUrl}, ${_editedProduct.id}');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Edit Product'),
        actions: <Widget>[
          IconButton(
            icon: const Icon(Icons.save),
            onPressed: _saveForm,
          )
        ],
      ),
      //handling user input with validation with Form widget, its invisible n doesn't render sometin on d screen u cud see but insde of d form, u can use special input widget which are den grouped 2geda n can b submitted 2geda n validated 2geda wit d help of flutter.
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Form(
          //using ds key 2 interract with d state managed by ds form widget, form is a stateful widget
          key: _form,
          //on autoValidate fire whenever u type someting into d form field
          //to make it scrollable, can slo use SingleChildScrollView
          child: ListView(
            children: <Widget>[
              //dey r connected 2geda with form
              TextFormField(
                //setting Initial Value (if wanna edit, loadin d product title hia)
                initialValue: _initValues['title'],
                decoration: const InputDecoration(
                  labelText: 'Title',
                  //adding border to form
                  border: OutlineInputBorder(),
                  //adding icon inside form
                  prefixIcon: Icon(Icons.title),
                ),
                textInputAction: TextInputAction.next,
                //when next button is click or entered we want 2 go 2 price field
                onFieldSubmitted: (_) {
                  FocusScope.of(context).requestFocus(_priceFocusNode);
                },
                validator: (value) {
                  if ('$value'.isEmpty) {
                    return 'Please provide a title!';
                  }
                  return null; //means input is correct n Valid
                  //return 'This is wrong'; //👈 this text is ur error message
                },
                onSaved: (value) {
                  _editedProduct = Product(
                    title: value.toString(),
                    price: _editedProduct.price,
                    description: _editedProduct.description,
                    imageUrl: _editedProduct.imageUrl,
                    id: _editedProduct.id,
                    isFavorite: _editedProduct.isFavorite,
                  );
                },
              ),
              const Divider(color: Colors.white),
              TextFormField(
                initialValue: _initValues['price'],
                decoration: const InputDecoration(
                  labelText: 'Price',
                  border: OutlineInputBorder(),
                  prefixIcon: Icon(Icons.payment),
                ),
                //Making input type 2 be Number Only
                textInputAction: TextInputAction.next,
                keyboardType: TextInputType.number,
                focusNode: _priceFocusNode,
                onFieldSubmitted: (_) {
                  FocusScope.of(context).requestFocus(_descriptionFocusNode);
                },
                validator: (value) {
                  if (value.toString().isEmpty) {
                    return 'Please enter a price!';
                  }
                  //mayb user enter string
                  if (double.tryParse('$value') == null) {
                    return 'Please enter a valid number!';
                  }
                  if (double.parse('$value') <= 0) {
                    return 'Please enter a number greater than zero!';
                  }
                  return null;
                },
                onSaved: (value) {
                  _editedProduct = Product(
                    title: _editedProduct.title,
                    price: double.parse(value.toString()),
                    description: _editedProduct.description,
                    imageUrl: _editedProduct.imageUrl,
                    id: _editedProduct.id,
                    isFavorite: _editedProduct.isFavorite,
                  );
                },
              ),
              const Divider(color: Colors.white),
              TextFormField(
                initialValue: _initValues['description'],
                decoration: const InputDecoration(
                  labelText: 'Description',
                  border: OutlineInputBorder(),
                  prefixIcon: Icon(Icons.description),
                ),
                maxLines: 3, //setting rows
                //give a keyboard suited for multiline
                keyboardType: TextInputType.multiline,
                focusNode: _descriptionFocusNode,
                validator: (value) {
                  if (value.toString().isEmpty) {
                    return 'Please enter a description!';
                  }
                  if ('$value'.length < 10) {
                    return 'Should be atleast 10 characters!';
                  }
                  return null;
                },
                onSaved: (value) {
                  _editedProduct = Product(
                    title: _editedProduct.title,
                    price: _editedProduct.price,
                    description: value.toString(),
                    imageUrl: _editedProduct.imageUrl,
                    id: _editedProduct.id,
                    isFavorite: _editedProduct.isFavorite,
                  );
                },
              ),
              const Divider(color: Colors.white),
              Row(
                crossAxisAlignment:
                    CrossAxisAlignment.end, //set input n container
                children: <Widget>[
                  Container(
                    width: 100,
                    height: 100,
                    margin: const EdgeInsets.only(top: 8, right: 10),
                    decoration: BoxDecoration(
                      border: Border.all(
                        width: 1,
                        color: Colors.grey,
                      ),
                    ),
                    child: _imageController.text.isEmpty
                        ? const Text('Enter a URL')
                        : FittedBox(
                            child: Image.network(
                              _imageController.text,
                              fit: BoxFit.cover,
                            ),
                          ),
                  ),
                  Expanded(
                    child: TextFormField(
                      //can't use initial value with controller same time
                      //initialValue: _initValues['imageUrl'],
                      decoration: const InputDecoration(labelText: 'Image Url'),
                      keyboardType: TextInputType.url,
                      textInputAction: TextInputAction.done,
                      //ds is updated wen we type into d text form field
                      controller: _imageController,
                      //when user loose focus or unselected it/wen go back to edit above field, i should preview so dt its not limited 2 wen dey submit / click ok
                      focusNode: _imageUrlFocusNode,
                      //when we click ok on d TextInputAction.done icon dt will be shown on d keyboard
                      onFieldSubmitted: (_) {
                        _saveForm();
                      },
                      validator: (value) {
                        if (value.toString().isEmpty) {
                          return 'Please enter an image URL!';
                        }
                        if (!'$value'.startsWith('http') &&
                            !'$value'.startsWith('https')) {
                          return 'Please enter a valid URL';
                        }
                        if (!'$value'.endsWith('.png') &&
                            !'$value'.endsWith('.jpg') &
                                !'$value'.endsWith('.jpeg')) {
                          return 'Please enter a valid image Url!';
                        }
                        return null;
                      },
                      onSaved: (value) {
                        _editedProduct = Product(
                          title: _editedProduct.title,
                          price: _editedProduct.price,
                          description: _editedProduct.description,
                          imageUrl: value.toString(),
                          id: _editedProduct.id,
                          isFavorite: _editedProduct.isFavorite,
                        );
                      },
                    ),
                  ),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
