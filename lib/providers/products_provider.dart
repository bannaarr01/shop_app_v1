import 'package:flutter/material.dart';
import './product.dart';

//We need to turn ds class into a data container
class Products with ChangeNotifier {
  //storing a list of products here in d Product class
  //ds property here _items wud neva b directly accessible 4rm outside
  //Not a final list cos it will change over time
  List<Product> _items = [
    Product(
      id: 'p1',
      title: 'Red Shirt',
      description: 'A red shirt - it is pretty red!',
      price: 29.99,
      imageUrl:
          'https://cdn.pixabay.com/photo/2016/10/02/22/17/red-t-shirt-1710578_1280.jpg',
    ),
    Product(
      id: 'p2',
      title: 'Trousers',
      description: 'A nice pair of trousers.',
      price: 59.99,
      imageUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/e/e8/Trousers%2C_dress_%28AM_1960.022-8%29.jpg/512px-Trousers%2C_dress_%28AM_1960.022-8%29.jpg',
    ),
    Product(
      id: 'p3',
      title: 'Yellow Scarf',
      description: 'Warm and cozy - exactly what you need for the winter.',
      price: 19.99,
      imageUrl:
          'https://live.staticflickr.com/4043/4438260868_cc79b3369d_z.jpg',
    ),
    Product(
      id: 'p4',
      title: 'A Pan',
      description: 'Prepare any meal you want.',
      price: 49.99,
      imageUrl:
          'https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/Cast-Iron-Pan.jpg/1024px-Cast-Iron-Pan.jpg',
    ),
  ];

//Filtering Logic n similar stuff should be handled in the widget
  // var _showFavoritesOnly = false;

  // // void showFavoritesOnly() {
  // //   _showFavoritesOnly = true;
  // //   notifyListeners();
  // // }

  // // void showAll() {
  // //   _showFavoritesOnly = false;
  // //   notifyListeners();
  // // }

  List<Product> get favoriteItems {
    return _items.where((prodItem) => prodItem.isFavorite).toList();
  }

  //Since it's private, den a getter dt rturn d list of products
  List<Product> get items {
    // if (_showFavoritesOnly) {
    //   //To a new list automatically
    //   return _items.where((prodItem) => prodItem.isFavorite).toList();
    // }

    //copy of d list _items
    return [..._items];
  }
  //all des objs in flutter were entered r reference type, If i would return my list items here, then i would return a pointer at ds object in memory which
  //grant direct access to d obj itsef in memory, den i can manipulate it anywhere else in d app which not wat i want cos wen my product change i actually have 2 call a certain method 2 tell all d listener of ds provider that new data is available. For instance👇

  void addProduct(Product product) {
    final newProduct = Product(
      title: product.title,
      description: product.description,
      price: product.price,
      imageUrl: product.imageUrl,
      id: DateTime.now().toString(), //dummy id, create ltr on d server
    );
    _items.add(newProduct); //OR 👇
    //_items.insert(0,newProduct);//add 2 d begining of product list
    //this annouce 📢 2 widgets which are listening 2 disclose n to changes in ds class , are then rebuilt and do actually get d LATEST data we have in there
    notifyListeners();
  }

//update existing product
  void updateProduct(String id, Product newProduct) {
    // execute a function on every product, return true if d Id we ar lookin at in our list of products is sam as id we are gettin here as arg
    final prodIndex = _items.indexWhere((prod) => prod.id == id);
    if (prodIndex >= 0) {
      //override the existing one with ds new update
      _items[prodIndex] = newProduct;
    } else {
      print('No existing product to update!');
    }
    notifyListeners();
  }

  void deleteProduct(String productId) {
    //execute ds function on all product n check d id if equal 2 d arg 2 remv 4rm list
    _items.removeWhere((prod) => prod.id == productId);
    notifyListeners();
  }
  //LET ALL CHANGES 2 DATA HAPPENS HERE so that we can use d notifier 2 trigger d announcement n update

  Product findById(String id) {
    return _items.firstWhere((prod) => prod.id == id);
  }
}

// A provider needs 2 be defined with the help of a class and u then build a 	new provider based on that class definition. A provider or ds class u
// 	create is also often referred to as a model because u also kind of 			modelling ur data there, d data in ur app, which then changes. U won’t
// 	just have a class but instead u need 2 add a mixin - a bit like extending	another class. The core diff is that u simply merge some properties or 		some methods in ur existing class, but u don’t return ur class into an
// 	instance of that inherited class. It’s a bit like inheritance Lite 2say.

// 	The class we need 2 mix here is ChangeNotifier class which is built into
// 	flutter which u enable my importing material dart package. It’s basically	kind of related to d inherited widget which the provider package uses be-
// 	hind d scenes n inherited widget, whilst we won’t work wit it directly, 		is simple a widget in flutter which allow us to establish the scenes comm	unification tunnels with the help of the context object we’re getting in
// 	every widget.
