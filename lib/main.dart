import 'package:flutter/material.dart';
//setting up provider
import 'package:provider/provider.dart';
import './screens/products_overview_screen.dart';
import './screens/product_detail_screen.dart';
import './screens/cart_screen.dart';
//setting up provider
import './providers/products_provider.dart';
import './providers/cart.dart';
import './providers/orders.dart';
import './screens/orders_screen.dart';
import './screens/user_products_screen.dart';
import './screens/edit_product_screen.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  MyApp({Key? key}) : super(key: key);
  final ThemeData theme = ThemeData();
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    //now wrap material app with ds. This allow us 2 register a class 2 which u can listen in child widgets and wenever that class updates d widgets which are listening, n ONLY 🟢 THESE (listening 🔊 child widgets/classes)🟢 are rebuild

    //Now wrapping with MultiProvider to allow providers as child
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          //now provide a builder method which rciv context n sud return NOT a widget,but a new instance of ur provided class. so here product
          create: (ctx) => Products(),
          //Products Provider
        ),
        ChangeNotifierProvider(
          create: (ctx) => Cart(),
          //Cart Provider
        ),
        ChangeNotifierProvider(
          create: (ctx) => Orders(),
          //Orders Provider
        ),
      ],
      //Now the child which is MaterialApp can tap into d providers
      //value: Products(),
      //Not d whole material app will rebuild but only those that LISTEN
      child: MaterialApp(
        title: 'Shop',
        // theme: ThemeData(
        //   primarySwatch: Colors.green,
        //   //colorScheme: secondary: Colors.deepPurple,
        //   colorScheme: colorScheme.copyWith(secondary: myColor),
        //   fontFamily: 'Lato',
        // ),
        theme: theme.copyWith(
          colorScheme: theme.colorScheme
              .copyWith(secondary: Colors.deepOrange, primary: Colors.green),
        ),
        home: ProductsOverviewScreen(),
        //HERE we have the route table 🚸 to register route, instead of passin unneeded data arnd widget
        routes: {
          ProductDetailScreen.routeName: (ctx) => ProductDetailScreen(),
          CartScreen.routeName: (ctx) => CartScreen(),
          OrdersScreen.routeName: (ctx) => OrdersScreen(),
          UserProductsScreen.routeName: (ctx) => UserProductsScreen(),
          EditProductScreen.routeName: (ctx) => EditProductScreen(),
        },
      ),
    );
  }
}
