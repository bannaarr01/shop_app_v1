import 'package:flutter/foundation.dart';

//define how cart will looke like \ blueprint of cart
class CartItem {
  final String id;
  final String title;
  final int quantity;
  final double price;

  CartItem({
    required this.id,
    required this.title,
    required this.quantity,
    required this.price,
  });
  //needs 2 map cartItem to d ID of the product they belong to. This ID here sud be  ID of the Cart Item, that is not the same as the ID of the product cos every cart item is not just a product, its a new object which allso contains information abou the quantity.
}

class Cart with ChangeNotifier {
  //Key = ProductId and value = CartItem
  Map<String, CartItem> _items = {};

  Map<String, CartItem> get items {
    return {..._items};
  }

  //movin logic here
  int get itemCount {
    return _items == {} ? 0 : _items.length;
  }

  double get totalAmount {
    var total = 0.0;
    _items.forEach((key, cartItem) {
      total += cartItem.price * cartItem.quantity;
    });
    return total;
  }

  void addItem(String productId, String title, double price) {
    //more importance of using productId as key
    if (_items.containsKey(productId)) {
      //if we already have d item in cart
      _items.update(
        productId,
        (existingCartItem) => CartItem(
          id: existingCartItem.id,
          title: existingCartItem.title,
          price: existingCartItem.price,
          quantity: existingCartItem.quantity + 1,
        ),
      );
    } else {
      _items.putIfAbsent(
          productId,
          () => CartItem(
                id: DateTime.now().toString(),
                title: title,
                price: price,
                quantity: 1,
                //1 wen we added for d first time
              ));
    }
    notifyListeners();
  }

  void removeSingleItem(String productId) {
    if (!_items.containsKey(productId)) {
      return;
    }
    if (_items[productId]!.quantity > 1) {
      _items.update(
        productId,
        (existingCartItem) => CartItem(
          id: existingCartItem.id,
          title: existingCartItem.title,
          price: existingCartItem.price,
          quantity: existingCartItem.quantity - 1,
        ),
      );
    } else {
      _items.remove(productId);
    }
    notifyListeners();
  }

//easiest way of removing item in a map
  void removeItem(String productId) {
    _items.remove(productId);
    notifyListeners();
  }

  //placing an order also means that the cart should be cleared
  void clearCart() {
    _items = {};
    notifyListeners();
  }
}
